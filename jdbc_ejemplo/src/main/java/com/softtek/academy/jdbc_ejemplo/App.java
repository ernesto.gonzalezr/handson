package com.softtek.academy.jdbc_ejemplo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.softtek.academia.JdbcExample.Color;


/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	List<Device> result = new ArrayList();
    	List<Device> listadevice2 = new ArrayList();
    	String SQL_SELECT="select * from device";
    	List<String> listaString =new ArrayList();
        try {
        	Conexion con=new Conexion();
        	Connection conectarse=con.getConnection();
            PreparedStatement preparedStatement = conectarse.prepareStatement(SQL_SELECT);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				
				Device dev=new Device(resultSet.getInt("deviceId"),resultSet.getString("NAME"),resultSet.getString("description"),resultSet.getInt("manufacturerid"),resultSet.getInt("colorid"),resultSet.getString("comments"));
            	result.add(dev);
              // System.out.println("ID: " + resultSet.getInt("deviceId") + " device name: " + resultSet.getString("NAME") + " desc value: " + resultSet.getString("description"));
            }
			
			listaString=result.stream()
					.map(Device::getName)
					.filter(h->h.equals("Laptop"))
				    .collect(Collectors.toList());
					
			listaString.forEach(System.out::println);
			long cuenta=result.stream()
			.map(Device::getManufacturerId)
			.filter(m->m==3).count();
			System.out.println("El resultado del conteo es: "+cuenta);
			
			listadevice2=result.stream()//guardar en otra lista Device las filas ColorId=1
				.filter(Device->Device.getColorId()==1)
				    .collect(Collectors.toList());
			
			listadevice2.stream().map(Device::getName)
			.forEach(System.out::println);
			
			
			  Map<Integer,Device> mimapa=result.stream()//convierte list to map
			             .collect(
			            		 Collectors.toMap(
			            				 Device::getDeviceId,Function.identity())
			            		 );
			   mimapa.forEach((k,v)->System.out.println("key: "+k+" value: "+v.getName()));//imprime el resultado
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        

        
      
    }
}
