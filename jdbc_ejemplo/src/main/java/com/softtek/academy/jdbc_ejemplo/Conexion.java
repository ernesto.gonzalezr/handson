package com.softtek.academy.jdbc_ejemplo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;



public class Conexion {
	private static Connection conn=null;
	
	public static Connection getConnection() {
		try {
				if(conn==null)
				{
					conn = DriverManager.getConnection(
					"jdbc:mysql://127.0.0.1:3306/sesion3?serverTimezone=UTC#", "root", "1234");
				}
			
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Failed to make connection!");
		}
		return conn;
		}
	
	static class getClose extends Thread {
		@Override 
		public void run() {
			try {
				Connection cone=Conexion.getConnection();
				cone.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
	}

}
	 