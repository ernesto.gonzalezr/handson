package com.softtek.academy.jdbc_ejemplo;

public class Device {

	private int deviceId;
	private String name;
	private String description;
	private int manufacturerId;
	private int colorId;
	private String comments;
	public Device(int deviceId, String name, String description, int manufacturerId, int colorId, String comments) {
	
		this.deviceId = deviceId;
		this.name = name;
		this.description = description;
		this.manufacturerId = manufacturerId;
		this.colorId = colorId;
		this.comments = comments;
	}
	
	public int getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(int deviceId) {
		this.deviceId = deviceId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getManufacturerId() {
		return manufacturerId;
	}
	public void setManufacturerId(int manufacturerId) {
		this.manufacturerId = manufacturerId;
	}
	public int getColorId() {
		return colorId;
	}
	public void setColorId(int colorId) {
		this.colorId = colorId;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}

	
}
